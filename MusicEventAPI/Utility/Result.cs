﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace MusicEventAPI.Utility
{
    public class Result
    {
        public bool Status { get; set; }
        internal HttpStatusCode StatusCode { get; set; }
        public dynamic Results { get; set; }
        public dynamic Group { get; set; }
        public string ErrorMessage { get; set; }
        //public ErrorInfo ErrorInfo { get; set; }
        public int totalPageCount { get; set; }
        public Result()
        {
            ErrorMessage = string.Empty;
            Group = new { };
            Results = new { };
            //ErrorInfo = new ErrorInfo() { Message = string.Empty };
        }

    }


}