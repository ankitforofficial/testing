﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace MusicEventAPI.Utility
{
    public static class Extension
    {
        //public static System.Security.SecureString ConvertToSecureString(this string password)
        //{
        //    if (password == null)
        //        throw new ArgumentNullException("password");
        //    unsafe
        //    {
        //        fixed (char* passwordChars = password)
        //        {
        //            var securePassword = new System.Security.SecureString(passwordChars, password.Length);
        //            securePassword.MakeReadOnly();
        //            return securePassword;
        //        }
        //    }
        //}
        public static string ToExecString(this SqlCommand cmd)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("EXEC {0} ", cmd.CommandText);

                List<KeyValuePair<string, object>> list = new List<KeyValuePair<string, object>>();

                foreach (SqlParameter param in cmd.Parameters)
                {
                    var val = GetDecoration(param.SqlDbType, param.Value);
                    list.Add(new KeyValuePair<string, object>(param.ParameterName, val));
                }

                sb.Append(list.Select(a => string.Format("{0} = {1}", a.Key, a.Value))
                     .Aggregate((a, b) => string.Format("{0}, {1}", a, b)));

                return sb.ToString();
            }
            catch
            {
                return "Error while generating exec statement";
            }
        }

        private static object GetDecoration(System.Data.SqlDbType sqlDbType, object val)
        {
            if (val == null || val == DBNull.Value)
                return "NULL";
            switch (sqlDbType)
            {
                case System.Data.SqlDbType.Char:
                case System.Data.SqlDbType.Date:
                case System.Data.SqlDbType.DateTime:
                case System.Data.SqlDbType.DateTime2:
                case System.Data.SqlDbType.DateTimeOffset:
                case System.Data.SqlDbType.NChar:
                case System.Data.SqlDbType.NText:
                case System.Data.SqlDbType.NVarChar:
                case System.Data.SqlDbType.SmallDateTime:
                case System.Data.SqlDbType.Text:
                case System.Data.SqlDbType.Time:
                case System.Data.SqlDbType.Timestamp:
                case System.Data.SqlDbType.UniqueIdentifier:
                case System.Data.SqlDbType.VarBinary:
                case System.Data.SqlDbType.VarChar:
                case System.Data.SqlDbType.Variant:
                case System.Data.SqlDbType.Xml:
                    return string.Format("'{0}'", val);
                default:
                    break;
            }
            return val;
        }

        public static string ToXML(this object obj, string elementName, XAttribute attribs)
        {
            if (string.IsNullOrWhiteSpace(elementName))
                return string.Empty;

            XElement listXE = null;

            if (obj is ICollection)
            {
                listXE = new XElement(elementName);

                if (attribs != null)
                {
                    listXE.Add(attribs);
                }

                foreach (var item in (obj as ICollection))
                {
                    CreateAttr(item, listXE);
                }

                return listXE.ToString();

            }
            else
            {
                listXE = new XElement(elementName);
                var xe = CreateAttr(obj, listXE);
                xe = new XElement(elementName, xe);
                return xe.ToString();
            }
        }

        public static string ToXML(this object obj, string elementName)
        {
            return ToXML(obj, elementName, null);
        }

        private static XElement CreateAttr(object obj, XElement listXE)
        {
            var xe = new XElement(obj.GetType().Name);
            listXE.Add(xe);

            foreach (var prop in obj.GetType().GetProperties())
            {
                if ((prop.GetValue(obj)) != null)
                {
                    var val = prop.GetValue(obj, null) ?? string.Empty;
                    xe.Add(new XAttribute(prop.Name, val));
                }
            }
            return xe;
        }
    }
}