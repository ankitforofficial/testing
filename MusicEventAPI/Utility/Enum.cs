﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Utility
{
    public enum CacheLogType
    {
        NewCache,
        ReuseCache
    }
    public class ErrorInfo
    {

        public string Message { get; set; }
    }

    public enum ErrorCode
    {
        NoAuthentication = -1,
        UserNameNotProvided = -2,
        PasswordNotProvided = -3,
        EventIDNotProvided = -4,
        DateNotProvided = -6,
        UserIDNotProvided = -7,
        TokenNotProviede = -8,
        EmailNotProvided = -9,
        GirftIDNotProviede = -10,
        Success = 0,
        UnKnown = -420,
        ConflictDBCallFailed = -100,
        FillResponseFailed = -101,
        invalidCredential = -11,
        otpsend = -15,
        InvalidMobile = -16,
        invalidOTP = -17,
        OTPExpired = -18,
        PhoneNoUsed = -19,
        EmailUsed = -20,
        GroupalreadyCreated = -21,
        NoEventRegistred = -22,
        NoUserRequestPending = -23,
        NouserWithThisGroup = -24,
        invalidAdmin = -25,
        alreadyJoinEvent = -26,
        noFriend = -27,
        Logout = -28,
        NOUSER = -29,
        HasTagExists = 30
    }
}