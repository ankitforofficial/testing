﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;

namespace MusicEventAPI.Utility
{
    public class ProcessingTask : BaseTask
    {
        public ProcessingTask(BaseRequest request, BaseResponse response)
            : base(request, response)
        {

        }

        protected override void ProcessOperation()
        {
            BuildResponse();
        }
    }
}