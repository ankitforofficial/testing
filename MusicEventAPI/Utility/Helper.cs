﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Utility
{
    public class Helper
    {
        internal static void FillResult(Result result, ErrorCode errorCode, dynamic obj,int totalpage =0,dynamic group = null)
        {
            if (group == null)
                group = new { };

            if (errorCode == ErrorCode.Success)
                result.Status = true;
            else
                result.Status = false;
            result.Results = new { };
            switch (errorCode)
            {
                case ErrorCode.Success:
                    result.StatusCode = System.Net.HttpStatusCode.OK;
                    result.Results = obj;
                    result.totalPageCount = totalpage;
                    result.Group = group;
                    break;
                case ErrorCode.NoAuthentication:
                    result.ErrorMessage = "infromation is not provided";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.invalidCredential:
                    result.ErrorMessage = "Username and password does not match ";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.FillResponseFailed:
                    result.ErrorMessage = "Information is failed fill response.";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.otpsend:
                    result.ErrorMessage = "Otp send successfully.";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.HasTagExists:
                    result.ErrorMessage = "Hastag already exists";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                    
                case ErrorCode.InvalidMobile:
                    result.ErrorMessage = "Phone number is invalid.";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.PhoneNoUsed:
                    result.ErrorMessage = "Phone number alreday used.";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.EmailUsed:
                    result.ErrorMessage = "Email alreday used.";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.invalidOTP:
                    result.ErrorMessage = "Please Check OTP.";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.OTPExpired:
                    result.ErrorMessage = "OTP was expired";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.GroupalreadyCreated:
                    result.ErrorMessage = "Same type of group is already exits.";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.NoEventRegistred:
                    result.ErrorMessage = "No Event Register with this event id";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.NoUserRequestPending:
                    result.ErrorMessage = "No user request is pending";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.NouserWithThisGroup:
                    result.ErrorMessage = "No user with Group";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;

                case ErrorCode.invalidAdmin:
                    result.ErrorMessage = "Invalid Admin";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.alreadyJoinEvent:
                    result.ErrorMessage = "user  already join this event";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.noFriend:
                    result.ErrorMessage = "No friend have this type of group";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.Logout:
                    result.ErrorMessage = "Logout";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case ErrorCode.NOUSER:
                    result.ErrorMessage = "You have not register with facebook, Please sign up.  ";
                    result.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    break;



            }

        }

        internal static int? SafeInt(object objInt)
        {
            int? myValue = Convert.IsDBNull(objInt) ? null : (int?)Convert.ToInt32(objInt);
            return myValue;
        }

        internal static short? SafeShort(object objShort)
        {
            short? myValue = Convert.IsDBNull(objShort) ? null : (short?)Convert.ToInt16(objShort);
            return myValue;
        }

        internal static DateTime? SafeDate(object objDate)
        {
            DateTime? myValue = Convert.IsDBNull(objDate) ? null : (DateTime?)Convert.ToDateTime(objDate);
            return myValue;
        }

        internal static decimal? SafeDecimal(object objDecimal)
        {
            decimal? myValue = Convert.IsDBNull(objDecimal) ? null : (decimal?)Convert.ToDecimal(objDecimal);
            return myValue;
        }

        internal static bool? SafeBool(object objBool)
        {
            bool? myValue = Convert.IsDBNull(objBool) ? null : (bool?)Convert.ToBoolean(objBool);
            return myValue;
        }
    }
}