﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;

namespace MusicEventAPI.Utility
{
    public static class GenrateOtp
    {

        public static string NewOTP()
        {
            //// string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            // // string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
            // string numbers = "1234567890";

            // string characters = numbers;
            // //if (rbType.SelectedItem.Value == "1")
            // //{
            // characters +=  numbers; //alphabets
            // //}
            // int length = 4;
            // string otp = string.Empty;
            // for (int i = 0; i < length; i++)
            // {
            //     string character = string.Empty;
            //     do
            //     {
            //         int index = new Random().Next(0, characters.Length);
            //         character = characters.ToCharArray()[index].ToString();
            //     } while (otp.IndexOf(character) != -1);
            //     otp += character;
            // }
            // return otp;

            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max).ToString();
        }
    }

    public static class GenratePassword
    {
     public static string NewPassword()
    {
        string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
        string numbers = "1234567890";

        string characters = numbers;
        //if (rbType.SelectedItem.Value == "1")
        //{
        characters += alphabets + numbers + small_alphabets;
        //}
        int length = 8;
        string otp = string.Empty;
        for (int i = 0; i < length; i++)
        {
            string character = string.Empty;
            do
            {
                int index = new Random().Next(0, characters.Length);
                character = characters.ToCharArray()[index].ToString();
            } while (otp.IndexOf(character) != -1);
            otp += character;
        }
        return otp;
    }
    }

    public static class SendSmsUser
    {
        public static void sms(string to ,string text)
        {

            //var wc = new System.Net.WebClient() { BaseAddress = "https://rest.nexmo.com/sms/json" };
            //wc.QueryString.Add("api_key", HttpUtility.UrlEncode("fcd991a5"));
            //wc.QueryString.Add("api_secret", HttpUtility.UrlEncode("ce9d88732e3a3744"));
            //wc.QueryString.Add("to", HttpUtility.UrlEncode(to));
            //wc.QueryString.Add("from", HttpUtility.UrlEncode("NexmoWorks"));
            //wc.QueryString.Add("text", HttpUtility.UrlEncode(text));
            //wc.DownloadString("");
            string AccountSid = "AC8c161e579684ad28f1bc03221748a4d8";
            string AuthToken = "b86ee3b3577a276953c35c5d5352f0d4";
            var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);

            var message = twilio.SendMessage(
                "+16572163416", to,
                text
            );
        }
    }

    public static class ErrorLog
    {
        
        internal static void AddLog(int UserID, string Message, int TemplateID)
        {
            try
            {

                using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection())
                {
                    con.ConnectionString = Manage.DBConnection.MusicEventConnectionString;
                    var cmd = con.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "CreateLog";
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@Message", Message);
                    cmd.Parameters.AddWithValue("@TemplateID", TemplateID);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                //ErrorLog.AddLog(1, ex.Message, 0);
            }

        }
    }

    public static class ImagePath
    {
        public static string GetimagePath(string path, string serverpath)
        {
            if (string.IsNullOrEmpty(path))
            {
                path = "/content/Image/NoImage.png";
            }

            if (path.Contains("http://") || path.Contains("https://"))
                return path;
            else
                return serverpath + path;
        } 
    }


}