﻿using MusicEventAPI.Areas.Admin.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Areas.Admin.Models
{
    public class AdminLogin
    {
        [Required(ErrorMessage = "Please Enter Email Id")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Enter Password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public static class CheckAdminAccess
    {
        public static bool CheckAdmin()
        {
            try
            {
                if (Convert.ToInt32(HttpContext.Current.Session["UserId"]) == 0)
                    return true;
                else
                {
                    if (Convert.ToBoolean(HttpContext.Current.Session["IsAdmin"]) == false)
                        return true;
                    return false;

                }
                    
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static bool CheckLogin()
        {
            try
            {
                if (Convert.ToInt32(HttpContext.Current.Session["UserId"]) == 0)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }


    public class jQueryTableModel
    {
        public string sEcho { get; set; }
        public string sSearch { get; set; }
        public int iDisplayLength { get; set; }
        public int iDisplayStart { get; set; }
        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }
        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }
        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }
        /// <summary>
        /// Sort column
        /// </summary>
        public int iSortCol_0 { get; set; }
        /// <summary>
        /// Asc or Desc
        /// </summary>
        public string sSortDir_0 { get; set; }
        /// <summary>
        /// fnServerparams, this should be an array of objects?
        /// </summary>  
        public string[] aoData { get; set; }
    }


}