//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MusicEventAPI.Areas.Admin.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ratings
    {
        public int RatingsID { get; set; }
        public Nullable<int> EventID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<decimal> Ratings1 { get; set; }
        public string ReviewText { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
