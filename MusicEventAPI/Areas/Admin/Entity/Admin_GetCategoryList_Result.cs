//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MusicEventAPI.Areas.Admin.Entity
{
    using System;
    
    public partial class Admin_GetCategoryList_Result
    {
        public string CategoryUniqueID { get; set; }
        public string CategoryName { get; set; }
        public bool ISActive { get; set; }
        public int CategoryID { get; set; }
        public string MainCategoryName { get; set; }
    }
}
