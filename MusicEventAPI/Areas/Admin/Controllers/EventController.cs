﻿using MusicEventAPI.Areas.Admin.Entity;
using MusicEventAPI.Areas.Admin.Models;
using MusicEventAPI.Areas.Admin.Repository;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicEventAPI.Areas.Admin.Controllers
{
    public class EventController : Controller
    {
        private EventRepository _repository = new EventRepository();
        // GET: Admin/Event
        public ActionResult Index()
        {
            if (CheckAdminAccess.CheckLogin())
                return RedirectToAction("", "Login", new { Area = "Admin" });
            return View();
        }

        [HttpGet]
        public ActionResult ManageEventSearchList(jQueryTableModel jModel, int role, int imagesvalue, string StartDate = null, string EndDate = null, string Keyword = null)
        {
            string path = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, "/");
            DateTime? fromdates = null, todates = null;
            if (!string.IsNullOrEmpty(StartDate))
            {
                fromdates = DateTime.ParseExact(StartDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                todates = DateTime.ParseExact(EndDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
            }

            if (jModel.aoData != null)
            {
                string lSearchValue = jModel.aoData[0].ToString();
            }

            //For sort start
            string sortBy = jModel.sColumns.Split(',').ToArray()[jModel.iSortCol_0];// + " " + jModel.sSortDir_0;
            var orderby = jModel.sSortDir_0;
            //For sort end
            int pageNumber = 1;
            if (jModel.iDisplayStart > 0)
            {
                pageNumber += (jModel.iDisplayStart / jModel.iDisplayLength);
            }
            var datelength = jModel.iDisplayLength;
            var data = _repository.GetPagingEventList(fromdates, todates, Keyword, role, pageNumber, datelength, sortBy, orderby.ToUpper(), imagesvalue);
            var TotalCount = Convert.ToInt32(data.Select(x => x.TotalRecords).FirstOrDefault());

            //For sort start
            //var pi = typeof(SP_WA_GetShiftList_Result).GetProperty(sortBy);
            //var orderByAddress = data.OrderBy(x => pi.GetValue(x, null));
            //if (orderby.ToLower() == "desc")
            //    orderByAddress = data.OrderByDescending(x => pi.GetValue(x, null));
            //For sort end
            var result = data.Select(row => new string[]
                {
                            "",
                            ImagePath.GetimagePath(row.Image,path),
                            row.CategoryName,
                            row.EventName,
                            row.StartDate,
                            row.EndDate,
                            row.latitude,
                            row.longitude,
                            row.TicketUrl,
                            Convert.ToString(row.EventID),



                });


            return Json(new
            {
                draw = Convert.ToInt32(jModel.sEcho),
                recordsTotal = TotalCount,
                recordsFiltered = TotalCount,
                data = result
            }, JsonRequestBehavior.AllowGet);

        }
        //public ActionResult loaddata(string StartDate = "", string EndDate = "", int role = 0)
        //{
        //    DateTime? sdate = null, Edate = null;
        //    if (!string.IsNullOrEmpty(StartDate))
        //        sdate = DateTime.ParseExact(StartDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
        //    if (!string.IsNullOrEmpty(EndDate))
        //        Edate = DateTime.ParseExact(EndDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);

        //    List<Admin_EventList_Result> data = _repository.GetEventlist(sdate,Edate, role).ToList();
        //    return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        public ActionResult Manage(int id = 0)
        {
            if (CheckAdminAccess.CheckLogin())
                return RedirectToAction("", "Login", new { Area = "Admin" });

            if (id == 0)
            {
                NewEvent1 model = new NewEvent1();
                model.StartDate = DateTime.Now;
                model.EndDate = DateTime.Now.AddMonths(1);
                return View(model);
            }
            else
            {
                NewEvent1 model = _repository.GetEventDetail(id);
                if (model == null)
                    return HttpNotFound();
                return View(model);
            }

        }

        [HttpPost]
        public ActionResult Manage(NewEvent1 model, HttpPostedFileBase Image, string StartDate = "", string EndDate = "")
        {
            if (CheckAdminAccess.CheckLogin())
                return RedirectToAction("", "Login", new { Area = "Admin" });

            if (!string.IsNullOrEmpty(StartDate))
                model.StartDate = DateTime.ParseExact(StartDate, "dd-MMMM-yyyy hh:mm tt", CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(EndDate))
                model.EndDate = DateTime.ParseExact(EndDate, "dd-MMMM-yyyy hh:mm tt", CultureInfo.InvariantCulture);

            string msg = (model.EventID == 0 ? "Add" : "Updated");


            model = _repository.ManageEvent(model, Image);
            if (model.EventID > 0 && model.EventID != -1)
            {
                TempData["message"] = "Event " + msg + " Successfully.";
                TempData["class"] = "alert alert-success";
                return RedirectToAction("Index");
            }
            else
                TempData["ErrorMessage"] = "Please try again.";

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeImage()
        {

            string Eventid = Convert.ToString(Request.Form["Eventid"]);
            string Filename = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    if (file != null && file.ContentLength > 0)
                    {

                        string path = Server.MapPath("~/Content/Image/Event/");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        string name = Guid.NewGuid().ToString() + ".Jpg";

                        //var byteArray = ms.ToArray();
                        path = Path.Combine(path, name);
                        if (Directory.Exists(Filename))
                        {
                            Directory.Delete(Filename);
                        }
                        Filename = "/Content/Image/Event/" + name;

                        try
                        {
                            file.SaveAs(path);
                            foreach (var item in Eventid.Split(','))
                            {
                                if (!string.IsNullOrEmpty(item))
                                    _repository.UploadImagePath(Filename, Convert.ToInt32(item));
                            }

                        }
                        catch (Exception ex) { }

                    }

                }

            }
            catch (Exception ex) { }

            return Content(Filename);
        }

        [HttpPost]
        public ActionResult ChangeCategory()
        {
            string Eventid = Convert.ToString(Request.Form["Eventid"]);
            int Categoryid = Convert.ToInt32(Request.Form["CategoryDDL"]);
            foreach (var item in Eventid.Split(','))
            {
                if (!string.IsNullOrEmpty(item))
                    _repository.UpdateCategory(Categoryid, Convert.ToInt32(item));
            }
            return Content("");
        }
    }
}