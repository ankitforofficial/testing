﻿using MusicEventAPI.Areas.Admin.Entity;
using MusicEventAPI.Areas.Admin.Models;
using MusicEventAPI.Areas.Admin.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicEventAPI.Areas.Admin.Controllers
{
    public class UserController : Controller
    {
        // GET: Admin/User
        private UserRepository repository = new UserRepository();
        public ActionResult Index()
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });
            return View();
        }

        public ActionResult loaddata(int status)
        {
            List<Admin_UsersList_Result> data = repository.GetUsers(status);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Manage(int id = 0)
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });

            if (id == 0)
            {
                User model = new User();
                return View(model);
            }
            else
            {
                User model = repository.GetUserDetail(id);
                if (model == null)
                    return HttpNotFound();
                return View(model);
            }

        }


        [HttpPost]
        public ActionResult Manage(User model, HttpPostedFileBase Image)
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });

            string msg = (model.UserID == 0 ? "Add" : "Updated");


            model = repository.ManageUser(model, Image);
            if (model.UserID > 0 && model.UserID != -1)
            {
                TempData["message"] = "User " + msg + " Successfully.";
                TempData["class"] = "alert alert-success";
                return RedirectToAction("Index");
            }
            else
                TempData["ErrorMessage"] = "Please try again.";

            return View(model);
        }
        public string SetFileName(string UserId)
        {

            string rootPath = Request.MapPath("/Content//Image//Event");
            string[] files = Directory.GetFiles(rootPath, "*.*", SearchOption.AllDirectories);

            // Display all the files.
            foreach (string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                //fileInfo.MoveTo(path + "NewUniqueFileNamHere");
                //System.IO.File.Move(fileInfo.FullName, fileInfo.FullName.Replace(" ", ""));

                string fileName = fileInfo.FullName;
                //string fileName1 = fileInfo.FullName.Replace(" ", "");


                string[] name = fileInfo.FullName.Split('\\');

                string a = string.Empty;


                for (int i = 0; i < name.Length - 1; i++)
                {
                    a = a + name[i] + "\\";
                }


                string b = name[name.Length - 1];

                Response.Write(a + "<br>");
                if (b.Contains(" "))
                {
                    string c = b.Replace(" ", "");

                    string d = a + b;
                    string e = a + c;

                    System.IO.File.Move(d, e);

                   

                }
            }





            return string.Empty;
        }
        [HttpPost]
        public ActionResult SetFileName(string UserId, string ParentId)
        {
            return View();
        }

    }
}