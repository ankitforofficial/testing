﻿using MusicEventAPI.Areas.Admin.Entity;
using MusicEventAPI.Areas.Admin.Models;
using MusicEventAPI.Areas.Admin.Repository;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicEventAPI.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        protected SignUpRepository _repository = new SignUpRepository();
        // GET: Manage/Login
        [HttpGet]
        public ActionResult Index()
        {
            AdminLogin model = new AdminLogin();
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(AdminLogin model)
        {
            if (ModelState.IsValid)
            {
                Security secu = new Security();
                User u = _repository.GetUser(model.Email, secu.Encrypt(model.Password));
                if (u == null)
                {
                    ModelState.AddModelError("", "Username or password does not match. ");
                }
                else
                {
                    Session["UserId"] = u.UserID;
                    Session["IsSubadmin"] = u.IsSubAdmin;
                    Session["IsAdmin"] = u.IsAdmin;
                    // _repository.SetSession(u);

                    if (u.IsSubAdmin == true)
                        return RedirectToAction("Index", "Event");
                    else
                       return RedirectToAction("Index", "Dashboard");
                }
                return View(model);
            }
            else
            {
                return View(model);
            }

        }

        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            var id = 0;
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });
            if (ModelState.IsValid)
            {

                id = _repository.ChangePassword(model, Convert.ToInt32(Session["UserId"]));

                if (id > 0)
                {
                    TempData["SuccessMessage"] = "Password Changed Successfully.";
                    return RedirectToAction("", "Dashboard");
                }
                ModelState.AddModelError("OldPassword", "Old Password Does not match ");
            }
            return View(model);
        }

        public ActionResult InsertData()
        {
            MusicEventAPI.Models.StoreData m = new MusicEventAPI.Models.StoreData();
            //  m.MoveData();
            m.GetMysql();
            return null;
        }
    }
}