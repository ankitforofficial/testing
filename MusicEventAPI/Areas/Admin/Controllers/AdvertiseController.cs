﻿using MusicEventAPI.Areas.Admin.Entity;
using MusicEventAPI.Areas.Admin.Models;
using MusicEventAPI.Areas.Admin.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicEventAPI.Areas.Admin.Controllers
{
    public class AdvertiseController : Controller
    {
        private EventRepository _repository = new EventRepository();
        // GET: Admin/Advertise
        public ActionResult Index()
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });
            return View();
        }

        public ActionResult loaddata(string StartDate = "", string EndDate = "",int status = 0)
        {
            DateTime? sdate = null, Edate = null;
            if (!string.IsNullOrEmpty(StartDate))
                sdate = DateTime.ParseExact(StartDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(EndDate))
                Edate = DateTime.ParseExact(EndDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);

            List<Admin_AdvertiseList_Result> data = _repository.GetAdvertiselist(sdate, Edate, status).ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Manage(int id = 0)
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });

            if (id == 0)
            {
                Advertisement model = new Advertisement();
                model.StartDate = DateTime.Now;
                model.EndDate = DateTime.Now.AddMonths(1);
                return View(model);
            }
            else
            {
                Advertisement model = _repository.GetAdvertisementDetail(id);
                if (model == null)
                    return HttpNotFound();
                return View(model);
            }

        }

        [HttpPost]
        public ActionResult Manage(Advertisement model, HttpPostedFileBase Image, string StartDate = "", string EndDate = "")
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });

            if (!string.IsNullOrEmpty(StartDate))
                model.StartDate = DateTime.ParseExact(StartDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(EndDate))
                model.EndDate = DateTime.ParseExact(EndDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);

            string msg = (model.Id == 0 ? "Add" : "Updated");


            model = _repository.ManageAdvertise(model,Image);
            if (model.Id > 0 && model.Id != -1)
            {
                TempData["message"] = "Advertise " + msg + " Successfully.";
                TempData["class"] = "alert alert-success";
                return RedirectToAction("Index");
            }
            else
                TempData["ErrorMessage"] = "Please try again.";

            return View(model);
        }
    }
}