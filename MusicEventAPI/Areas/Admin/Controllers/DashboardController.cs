﻿using MusicEventAPI.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicEventAPI.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });
            return View();
        }
    }
}