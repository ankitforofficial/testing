﻿using MusicEventAPI.Areas.Admin.Entity;
using MusicEventAPI.Areas.Admin.Models;
using MusicEventAPI.Areas.Admin.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicEventAPI.Areas.Admin.Controllers
{
    public class MainCategoryController : Controller
    {
        private MainCategoryRepository repository = new MainCategoryRepository();
        // GET: Admin/Category
        public ActionResult Index()
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });
            return View();
        }

        public ActionResult loaddata(int status)
        {
            List<Admin_MainGetCategoryList_Result> data = repository.GetCategory(status);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Manage(int id = 0)
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });

            if (id == 0)
            {
                MainCategory model = new MainCategory();
                return View(model);
            }
            else
            {
                MainCategory model = repository.GetCategoryDetail(id);
                if (model == null)
                    return HttpNotFound();
                return View(model);
            }

        }



        [HttpPost]
        public ActionResult Manage(MainCategory model)
        {
            if (CheckAdminAccess.CheckAdmin())
                return RedirectToAction("", "Login", new { Area = "Admin" });
            string msg = (model.MainCatID == 0 ? "Add" : "Updated");
            model = repository.ManageCategory(model);
            if (model.MainCatID > 0 && model.MainCatID != -1)
            {
                TempData["message"] = "Sub Category " + msg + " Successfully.";
                TempData["class"] = "alert alert-success";
                return RedirectToAction("Index");
            }
            else
                TempData["ErrorMessage"] = "Please try again.";

            return View(model);
        }

    }
}