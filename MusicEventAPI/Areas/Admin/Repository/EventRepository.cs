﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicEventAPI.Areas.Admin.Entity;
using System.IO;
using MusicEventAPI.Areas.Admin.Models;

namespace MusicEventAPI.Areas.Admin.Repository
{
    public class EventRepository:BaseAccessor
    {
        public List<Admin_EventList_Result> GetEventlist(DateTime? startdate,DateTime? enddate,int role)
        {
            return _db.Admin_EventList(role, startdate, enddate).ToList();
        }

        public List<Admin_AdvertiseList_Result>  GetAdvertiselist(DateTime? sdate, DateTime? edate,int status)
        {
            return _db.Admin_AdvertiseList(sdate, edate, status).ToList();
        }

        public Advertisement GetAdvertisementDetail(int id)
        {
            return _db.Advertisement.FirstOrDefault(x => x.Id == id);
        }

        public Advertisement ManageAdvertise(Advertisement model, HttpPostedFileBase file)
        {
            Advertisement _A = new Advertisement();
            try
            {

                if (model.Id > 0)
                    _A = _db.Advertisement.FirstOrDefault(x => x.Id == model.Id);
                if (_A != null)
                {
                    _A.Name = model.Name;
                    _A.IsActive = model.IsActive;
                    _A.StartDate = model.StartDate;
                    _A.EndDate = model.EndDate;
                    _A.PhoneNo = model.PhoneNo;
                    _A.Discription = model.Discription;
                    _A.Discount = model.Discount;
                    _A.Address = model.Address;
                    _A.Latitude = model.Latitude;
                    _A.Longitude = model.Longitude;
                    _A.IsGlobal = model.IsGlobal;
                    _A.URL = model.URL;

                    if (model.Id == 0)
                    {
                        _db.Advertisement.Add(_A);
                    }
                    _db.SaveChanges();

                    if (file != null)
                    {
                        string fileName = string.Empty;
                        string path = HttpContext.Current.Server.MapPath("~/Content/Image/Advertise/") + _A.Id;
                        if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            _A.ImagePath = "/Content/Image/Advertise/" + _A.Id + "/" + testfiles[testfiles.Length - 1];
                            fileName = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            _A.ImagePath = "/Content/Image/Advertise/" + _A.Id + "/" + file.FileName;
                            fileName = file.FileName;
                        }
                        

                        _db.SaveChanges();
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        fileName = Path.Combine(path, fileName);
                        file.SaveAs(fileName);
                    }

                    return _A;
                }
            }
            catch (Exception ex)
            {
                _A.Id = -1;
            }
            return _A;
        }

        public NewEvent1 ManageEvent(NewEvent1 model, HttpPostedFileBase file)
        {
            NewEvent _E = new NewEvent();
            try
            {
                if (model.EventID > 0)
                    _E = _db.NewEvent.FirstOrDefault(x => x.EventID == model.EventID);
                if (_E != null)
                {
                    _E.EventName = model.EventName;
                    _E.CategoryID = model.CategoryID;
                    _E.ISActive = model.ISActive;
                    _E.StartDate = model.StartDate;
                    _E.EndDate = model.EndDate;
                    _E.Starttime = model.StartDate.Value.TimeOfDay;
                    _E.Endtime = model.EndDate.Value.TimeOfDay;

                    _E.latitude = model.latitude;
                    _E.longitude = model.longitude;
                    // _E.EventDiscription = model.EventDiscription;
                    _E.location = model.location;
                    _E.IsTmaster = model.IsTmaster;
                    _E.TicketUrl = model.TicketUrl;

                    if (model.EventID == 0)
                    {
                        _db.NewEvent.Add(_E);
                    }
                    _db.SaveChanges();
                    EventDetail ed = new EventDetail();
                    if (model.EventID > 0)
                        ed = _db.EventDetail.FirstOrDefault(x => x.EventId == model.EventID);
                    if (ed == null)
                        ed = new EventDetail();
                    ed.EventId = model.EventID;
                    ed.Discription = model.EventDiscription;
                    if (ed.Id == 0)
                        _db.EventDetail.Add(ed);
                    _db.SaveChanges();

                    if (file != null)
                    {
                        string fileName = string.Empty;
                        string path = HttpContext.Current.Server.MapPath("~/Content/Image/Event/") + _E.EventID;
                        if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            _E.Image = "/Content/Image/Event/" + _E.EventID + "/" + testfiles[testfiles.Length - 1];
                            fileName = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            _E.Image = "/Content/Image/Event/" + _E.EventID + "/" + file.FileName;
                            fileName = file.FileName;
                        }
                        _db.SaveChanges();
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        fileName = Path.Combine(path, fileName);
                        file.SaveAs(fileName);
                    }
                    model.EventID = _E.EventID;
                    return model;
                }
            }
            catch (Exception ex)
            {
                model.EventID = -1;
            }
            return model;
        }

        public NewEvent1 GetEventDetail(int id)

        {
            NewEvent data = _db.NewEvent.FirstOrDefault(x => x.EventID == id);
            NewEvent1 model = new NewEvent1();
            model.EventID = data.EventID;
            model.UserID = data.UserID;
            model.CategoryID = data.CategoryID;
            model.EventName = data.EventName;
            model.StartDate = data.StartDate;
            model.EndDate = data.EndDate;
            model.Starttime = data.Starttime;
            model.Endtime = data.Endtime;
            model.Dealtext = data.Dealtext;
            model.Percentage = data.Percentage;
            model.latitude = data.latitude;
            model.longitude = data.longitude;
            model.location = data.location;
            model.Image = data.Image;
            model.ISActive = data.ISActive;
            model.CreatedBy = data.CreatedBy;
            model.UpdatedBy = data.UpdatedBy;
            model.CreatedDatetime = data.CreatedDatetime;
            model.UpdatedDatetime = data.UpdatedDatetime;
            model.TicketUrl = data.TicketUrl;
            model.IsTmaster = data.IsTmaster;
            model.MasterEventID = data.MasterEventID;
            model.ViewCount = data.ViewCount;
            if (data != null)
            {
                try
                {
                    model.EventDiscription = _db.EventDetail.FirstOrDefault(x => x.EventId == id).Discription;
                }
                catch (Exception ex) { }
            }
            return model;
        }

        public List<Admin_EventList_Paging_Result> GetPagingEventList(DateTime? startdate, DateTime? enddate, string keyword, int category, int pageNumber, int pageSize, string sortCol, string sortDir,int imagesvalue)
        {
            if (keyword == "") keyword = null; else keyword = keyword.ToLower();
            return _db.Admin_EventList_Paging(keyword, category, startdate, enddate, pageNumber, pageSize, sortCol, sortDir, imagesvalue).ToList();
        }


        public void UploadImagePath(string path, int id)
        {
            NewEvent E = _db.NewEvent.FirstOrDefault(x => x.EventID == id);
            if (E != null)
            {
                E.Image = path;
                _db.SaveChanges();
            }
        }
        public void UpdateCategory(int Catid, int id)
        {
            NewEvent E = _db.NewEvent.FirstOrDefault(x => x.EventID == id);
            if (E != null)
            {
                E.CategoryID = Catid;
                _db.SaveChanges();
            }
        }

    }
}