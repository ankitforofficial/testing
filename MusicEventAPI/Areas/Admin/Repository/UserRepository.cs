﻿using MusicEventAPI.Areas.Admin.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Areas.Admin.Repository
{
    public class UserRepository : BaseAccessor
    {
        public List<Admin_UsersList_Result> GetUsers(int status)
        {
            return _db.Admin_UsersList(status).ToList();
        }

        public User GetUserDetail(int id)
        {
            return _db.User.FirstOrDefault(x => x.UserID == id);
        }

        public User ManageUser(User model, HttpPostedFileBase file)
        {
            User _U = new User();
            try
            {

                if (model.UserID > 0)
                    _U = _db.User.FirstOrDefault(x => x.UserID == model.UserID);
                if (_U != null)
                {
                    _U.Name = model.Name;
                    _U.IsActive = model.IsActive;
                    _U.IsAdmin = model.IsAdmin;
                    _U.IsSubAdmin = model.IsSubAdmin;
                    _U.UserName = model.UserName;
                    _U.IsBusinessUser = model.IsBusinessUser;


                    if (model.UserID == 0)
                    {
                        _db.User.Add(_U);
                    }
                    _db.SaveChanges();

                    if (file != null)
                    {
                        string path = HttpContext.Current.Server.MapPath("~/Content/Image/User/") + _U.UserID;

                        _U.UserImagePath = "/Content/Image/User/" + _U.UserID + "/ProfilPic.Jpg";

                        _db.SaveChanges();
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        string fileName = Path.Combine(path, "ProfilPic.Jpg");
                        file.SaveAs(fileName);
                    }

                    return _U;
                }
            }
            catch (Exception ex)
            {
                _U.UserID = -1;
            }
            return _U;
        }
    }
}