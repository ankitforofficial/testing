﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicEventAPI.Areas.Admin.Entity;

namespace MusicEventAPI.Areas.Admin.Repository
{
    public class BaseAccessor : IDisposable
    {

        protected readonly MusicEventEntities _db = new MusicEventEntities();

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}