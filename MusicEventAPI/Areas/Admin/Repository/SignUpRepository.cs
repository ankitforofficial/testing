﻿using MusicEventAPI.Areas.Admin.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicEventAPI.Areas.Admin.Models;

namespace MusicEventAPI.Areas.Admin.Repository
{
    public class SignUpRepository:BaseAccessor
    {
        public User GetUser(string Username, string password)
        {
            try
            {
                return _db.User.FirstOrDefault(x => x.Email == Username && x.Password == password && (x.IsAdmin == true || x.IsSubAdmin == true));
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public int ChangePassword(ChangePasswordViewModel model, int id)
        {
            Utility.Security _Encrypt = new Utility.Security();
             int retval = 0;
            model.OldPassword = _Encrypt.Encrypt(model.OldPassword);
            var admin = _db.User.FirstOrDefault(x => x.UserID == id && x.Password == model.OldPassword);
            if (admin != null)
            {
                admin.Password = _Encrypt.Encrypt(model.NewPassword);
                _db.SaveChanges();
                retval = admin.UserID;
            }
            return retval;
        }
    }
}