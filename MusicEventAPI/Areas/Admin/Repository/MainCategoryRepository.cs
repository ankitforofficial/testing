﻿using MusicEventAPI.Areas.Admin.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Areas.Admin.Repository
{
    public class MainCategoryRepository : BaseAccessor
    {
        public List<Admin_MainGetCategoryList_Result> GetCategory(int status)
        {
            return _db.Admin_MainGetCategoryList(status).ToList();
        }

        public MainCategory GetCategoryDetail(int id)
        {
            return _db.MainCategory.FirstOrDefault(x => x.MainCatID == id);
        }

        public MainCategory ManageCategory(MainCategory model)
        {
            MainCategory _c = new MainCategory();
            try
            {

                if (model.MainCatID > 0)
                    _c = _db.MainCategory.FirstOrDefault(x => x.MainCatID == model.MainCatID);
                if (_c != null)
                {
                    _c.MainCategoryName = model.MainCategoryName;
                    _c.IsActive = model.IsActive;

                    if (model.MainCatID == 0)
                    {
                        _db.MainCategory.Add(_c);
                    }
                    _db.SaveChanges();

                    return _c;
                }
            }
            catch (Exception ex)
            {
                _c.MainCatID = -1;
            }
            return _c;
        }
    }

}