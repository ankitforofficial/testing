﻿using MusicEventAPI.Areas.Admin.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Areas.Admin.Repository
{
    public static class ConstantRepository
    {
       public static  MusicEventEntities _db = new MusicEventEntities();

        public static List<Category> GetCategoryList()
        {
            List<Category> cat = new List<Category>();

            cat.Add(new Category
            {
                CategoryID = 0,
                CategoryName = "All"
            });
            cat.AddRange(_db.Category.ToList());
            return cat;
        }
        public static List<Category> GetListCategory()
        {
            return _db.Category.ToList();
        }

        public static List<MainCategory> GetMainCategory()
        {
            return _db.MainCategory.ToList();
        }

        public static Admin_GetDashboardCount_Result GetDashboardCount()
        {
            return _db.Admin_GetDashboardCount().FirstOrDefault();
        }
    }
}