﻿using MusicEventAPI.Areas.Admin.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Areas.Admin.Repository
{
    public class CategoryRepository : BaseAccessor
    {
        public List<Admin_GetCategoryList_Result> GetCategory(int status)
        {
            return _db.Admin_GetCategoryList(status).ToList();
        }

        public Category GetCategoryDetail(int id)
        {
            return _db.Category.FirstOrDefault(x => x.CategoryID == id);
        }

        public Category ManageCategory(Category model)
        {
            Category _c = new Category();
            try
            {

                if (model.CategoryID > 0)
                    _c = _db.Category.FirstOrDefault(x => x.CategoryID == model.CategoryID);
                if (_c != null)
                {
                    _c.CategoryName = model.CategoryName;
                    _c.ISActive = model.ISActive;
                    _c.MainCatID = model.MainCatID;

                    if (model.CategoryID == 0)
                    {
                        _c.CreatedDatetime = DateTime.Now;
                        _db.Category.Add(_c);
                    }
                    _c.UpdatedDatetime = DateTime.Now;
                    _db.SaveChanges();

                    return _c;
                }
            }
            catch (Exception ex)
            {
                _c.CategoryID = -1;
            }
            return _c;
        }
    }

}