﻿using MusicEventAPI.Models.Request;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MusicEventAPI.ActionFilters
{
    public class AuthorizationRequiredAttribute : ActionFilterAttribute
    {
        public string request { get; set; }
        //private const string Token = "SessionGUId";

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            var modelState = filterContext.ModelState;
            if (!modelState.IsValid)
                filterContext.Response = filterContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, modelState);

            BaseRequest request = (BaseRequest)filterContext.ActionArguments["request"];
            //if (!new SmartMapWebApi.Service.SmartMapRepository().Authenticate(request))
            //{
            //    var responseMessage = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
            //    filterContext.Response = responseMessage;
            //}
            base.OnActionExecuting(filterContext);
        }


        //public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        //{
        //var allowOrigin = "*";
        //if (HttpContext.Current.Request.UrlReferrer != null)
        //{
        //    var origin = HttpContext.Current.Request.UrlReferrer.ToString().Substring(0, HttpContext.Current.Request.UrlReferrer.ToString().Length - 1);
        //    allowOrigin = !string.IsNullOrWhiteSpace(origin) ? origin : "*";
        //    filterContext.Request.Headers.Add("Access-Control-Allow-Origin", allowOrigin);
        //}
        //else
        //    filterContext.Request.Headers.Add("Access-Control-Allow-Origin", allowOrigin);

        //    var allowOrigin = "*";
        //    if (HttpContext.Current.Request.UrlReferrer != null)
        //    {
        //        var origin = HttpContext.Current.Request.UrlReferrer.ToString().Substring(0, HttpContext.Current.Request.UrlReferrer.ToString().Length - 1);
        //        allowOrigin = !string.IsNullOrWhiteSpace(origin) ? origin : "*";
        //    }
        //    if (actionExecutedContext.Response != null)
        //        actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Origin", allowOrigin);

        //    base.OnActionExecuted(actionExecutedContext);
        //}
    }
}