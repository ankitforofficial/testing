﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Service
{
    public abstract class BaseTask
    {
        protected BaseRequest request;
        protected BaseResponse response;

        public BaseTask(BaseRequest request, BaseResponse response)
        {
            this.request = request;
            this.response = response;
            this.response.Result = new Result();
        }

        protected abstract void ProcessOperation();


        protected void BuildResponse()
        {

            //Fill response
            response.Fill(request);


        }

        public virtual BaseResponse CreateResponse()
        {
            try
            {
                //Log request to database [structure needs to be defined]


                //It can be Put / Get / Post
                ProcessOperation();


            }
            catch (Exception ex)
            {
                response.Result.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                //response.Result.ErrorInfo.Message = string.Format("{0}", ex.Message);
                response.Result.ErrorMessage = string.Format("{0}", ex.Message);
            }

            return response;
        }
    }
}