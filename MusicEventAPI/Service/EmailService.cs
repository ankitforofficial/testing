﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;

namespace MusicEventAPI.Service
{
    public class EmailService
    {
        #region Private Properties
        private static string _networkhost { get; set; }
        private static string _port { get; set; }
        private static string _fromMail { get; set; }
        private static string _userName { get; set; }
        private static string _password { get; set; }
        private static string _FromCc { get; set; }
        private static string _displayName { get; set; }
        private static bool _useDefaultCredentials { get; set; }
        private static bool _enableSsl { get; set; }
        private static bool _isBodyHtml { get; set; }


        #endregion

        #region Constructor
        public EmailService()
        {
            _networkhost = WebConfigurationManager.AppSettings["smtpServer"].ToString();
            _port = WebConfigurationManager.AppSettings["smtpPort"].ToString();
            _fromMail = WebConfigurationManager.AppSettings["smtpFrom"].ToString();
            _userName = WebConfigurationManager.AppSettings["smtpUser"].ToString();
            _password = WebConfigurationManager.AppSettings["smtpPass"].ToString();
            _FromCc = WebConfigurationManager.AppSettings["smtpCC"].ToString();
            _useDefaultCredentials = Convert.ToBoolean(WebConfigurationManager.AppSettings["DefaultCredentials"].ToString());
            _enableSsl = Convert.ToBoolean(WebConfigurationManager.AppSettings["EnableSsl"].ToString());
            _isBodyHtml = Convert.ToBoolean(WebConfigurationManager.AppSettings["IsBodyHtml"].ToString());
            _displayName = WebConfigurationManager.AppSettings["smtpdisplayName"].ToString();
        }
        #endregion

        #region Public Methods
        public bool SendEmailAsync(string emailTo, string mailbody, string subject)
        {

            var from = new MailAddress(_fromMail, _displayName);
            var to = new MailAddress(emailTo);
            using (var mail = new MailMessage(from, to))
            {
                mail.Subject = subject;
                mail.Body = mailbody;
                mail.IsBodyHtml = _isBodyHtml;
                if (!string.IsNullOrEmpty(_FromCc))
                    mail.CC.Add(new MailAddress(_FromCc, "My Email"));



                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.Delay |
                                                   DeliveryNotificationOptions.OnFailure |
                                                   DeliveryNotificationOptions.OnSuccess;

                using (var client = new SmtpClient())
                {
                    client.Host = _networkhost;
                    client.EnableSsl = _enableSsl;
                    client.Port = Convert.ToInt16(_port);
                    client.UseDefaultCredentials = _useDefaultCredentials;

                    if (!client.UseDefaultCredentials && !string.IsNullOrEmpty(_userName) &&
                        !string.IsNullOrEmpty(_password))
                    {
                        client.Credentials = new NetworkCredential(_userName, _password);
                    }

                    client.SendMailAsync(mail);
                }
            }

            return true;
        }

        public bool Godaddysendmail(string towho, string subject, string bodyStr)
        {
            try
            {
                string fromemail = _userName;
                MailMessage mail = new MailMessage(fromemail, towho, subject, bodyStr);
                mail.IsBodyHtml = _isBodyHtml;
                if (!string.IsNullOrEmpty(_FromCc))
                    mail.CC.Add(new MailAddress(_FromCc, "My Email"));

                SmtpClient smtp = new SmtpClient();


                if (_networkhost != null && _port != null)
                {
                    int Port = Convert.ToInt32(_port);
                    smtp = new SmtpClient(_networkhost, Port);  //Set SMTP SERVER AND PORT NUMBER
                    smtp.EnableSsl = _enableSsl;
                    smtp.Credentials = new NetworkCredential(_userName, _password);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(mail);   //Send Mail
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion



        #region Private methods

        private void SendMail(string from, string body, string subject)
        {
            string Username = WebConfigurationManager.AppSettings["PFUserName"].ToString();
            string Password = WebConfigurationManager.AppSettings["PFPassWord"].ToString();
            string MailServer = WebConfigurationManager.AppSettings["MailServerName"].ToString();
            NetworkCredential cred = new NetworkCredential(Username, Password);
            string mailServerName = ("smtp.gmail.com,587");

            MailMessage message = new MailMessage(from, Username, subject, body);
            SmtpClient mailClient = new SmtpClient("smtp.gmail.com,587");
            mailClient.EnableSsl = true;

            mailClient.Host = mailServerName;
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = cred;
            mailClient.Send(message);
            message.Dispose();

        }
        #endregion
    }
}