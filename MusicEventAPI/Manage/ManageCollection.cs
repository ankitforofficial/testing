﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MusicEventAPI.Manage
{
    public static class ManageCollection
    {
        public static List<T> ToCollection<T>(this DataTable dt)
        {
            List<T> lst = new System.Collections.Generic.List<T>();
            Type ClassType = typeof(T);
            PropertyInfo[] pClass = ClassType.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();
            T cn;
            foreach (DataRow item in dt.Rows)
            {
                cn = (T)Activator.CreateInstance(ClassType);
                foreach (PropertyInfo pc in pClass)
                {
                    // Can comment try catch block. 
                    try
                    {
                        DataColumn d = dc.Find(c => c.ColumnName.Trim().ToLower() == pc.Name.Trim().ToLower());
                        if (d != null)
                        {
                            pc.SetValue(cn, item[pc.Name], null);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

                lst.Add(cn);
            }
            return lst;
        }

        public static T ToObject<T>(this DataTable dt)
        {
            Type ClassType = typeof(T);
            PropertyInfo[] pClass = ClassType.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();
            T cn;
            DataRow dr = dt.Rows[0];
            cn = (T)Activator.CreateInstance(ClassType);
            foreach (PropertyInfo pc in pClass)
            {
                try
                {
                    DataColumn d = dc.Find(c => c.ColumnName.Trim().ToLower() == pc.Name.Trim().ToLower());
                    if (d != null)
                    {
                        pc.SetValue(cn, dr[pc.Name], null);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return cn;
        }

    }
}