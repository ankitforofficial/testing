﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MusicEventAPI.Manage
{
    public static class DBConnection
    {
        public static string MusicEventConnectionString
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["MusicEventConnectionString"].ToString(); }
        }
      
    }
}