﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
namespace MusicEventAPI.Controllers
{
    public class CreateGroupController : BaseController
    {
    
        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> Post()
        {

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Content/BodyPart");
            var provider = new MultipartFormDataStreamProvider(root);
            CreateGroupRequest _request = new CreateGroupRequest();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (MultipartFileData file in provider.FileData)
            {

                _request.image = file.LocalFileName;
                // _request.FileName = file.Headers.ContentDisposition.FileName;

            }
            if (provider.FormData.AllKeys.Contains("groupId"))
                _request.groupId = Convert.ToInt32(provider.FormData.GetValues("groupId")[0]);
            _request.userId = Convert.ToInt32(provider.FormData.GetValues("userId")[0]);
            _request.groupName = Convert.ToString(provider.FormData.GetValues("groupName")[0]);
            _request.tagName = provider.FormData.GetValues("tagName")[0];
            _request.groupType = provider.FormData.GetValues("groupType")[0];

            if (provider.FormData.AllKeys.Contains("accessToken"))
                _request.accessToken = provider.FormData.GetValues("accessToken")[0];

            BaseTask task = new ProcessingTask(_request, new CreateGroupResponce());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);

        }
    }
}
