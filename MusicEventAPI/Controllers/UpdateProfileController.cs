﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class UpdateProfileController : BaseController
    {
        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> Post()
        {

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Content/BodyPart");
            var provider = new MultipartFormDataStreamProvider(root);
            UpdateProfileRequest _request = new UpdateProfileRequest();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (MultipartFileData file in provider.FileData)
            {

                _request.FileName = file.LocalFileName;
               // _request.FileName = file.Headers.ContentDisposition.FileName;

            }

            _request.name = provider.FormData.GetValues("name")[0];
            _request.userName = provider.FormData.GetValues("userName")[0];
            _request.groupType = provider.FormData.GetValues("groupType")[0];
            if (provider.FormData.AllKeys.Contains("groupPeople"))
                _request.groupPeople = provider.FormData.GetValues("groupPeople")[0];
            if (provider.FormData.AllKeys.Contains("emailId"))
                _request.emailId = provider.FormData.GetValues("emailId")[0];
            _request.userID = Convert.ToInt32(provider.FormData.GetValues("userID")[0]);
            if (provider.FormData.AllKeys.Contains("filepath"))
                _request.filepath = provider.FormData.GetValues("filepath")[0];
            if (provider.FormData.AllKeys.Contains("password"))
                _request.password = provider.FormData.GetValues("password")[0];
            if (provider.FormData.AllKeys.Contains("accessToken"))
                _request.accessToken = provider.FormData.GetValues("accessToken")[0];

            BaseTask task = new ProcessingTask(_request, new UpdateProfileResponse());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);

        }
    }
}

