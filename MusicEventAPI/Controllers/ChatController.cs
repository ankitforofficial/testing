﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class ChatController : BaseController
    {

        // GET: test
        [System.Web.Http.HttpPost]
        public IHttpActionResult Post([FromBody] ChatRequest request)
        {
            BaseTask task = new ProcessingTask(request, new ChatResponse());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);
        }
    }
}
