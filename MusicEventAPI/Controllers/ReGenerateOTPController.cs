﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class ReGenerateOTPController : BaseController
    {
        [System.Web.Http.HttpPost]
        public IHttpActionResult Post([FromBody] ReGenrateOTPRequest request)
        {
            BaseTask task = new ProcessingTask(request, new ReGenrateOTPResponse());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);
        }
    }
}
