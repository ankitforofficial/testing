﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class CreateActivityController : BaseController
    {
        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> Post()
        {

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Content/BodyPart");
            var provider = new MultipartFormDataStreamProvider(root);
            CreateActivityRequest _request = new CreateActivityRequest();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (MultipartFileData file in provider.FileData)
            {

                _request.FileName = file.LocalFileName;
                // _request.FileName = file.Headers.ContentDisposition.FileName;

            }
            if (provider.FormData.AllKeys.Contains("activityId"))
                _request.activityId = Convert.ToInt32(provider.FormData.GetValues("activityId")[0]);
            _request.userID = Convert.ToInt32(provider.FormData.GetValues("userID")[0]);
            _request.eventId = Convert.ToInt32(provider.FormData.GetValues("eventId")[0]);
            _request.activityName = provider.FormData.GetValues("activityName")[0];
            _request.location = provider.FormData.GetValues("location")[0];
            _request.startDate = provider.FormData.GetValues("startDate")[0];
            _request.endDate = provider.FormData.GetValues("endDate")[0];
            _request.startTime = provider.FormData.GetValues("startTime")[0];
            _request.endTime = provider.FormData.GetValues("endTime")[0];
            _request.activityDescription = provider.FormData.GetValues("activityDescription")[0];
            if (provider.FormData.AllKeys.Contains("filepath"))
                _request.filepath = provider.FormData.GetValues("filepath")[0];

            _request.latitude = provider.FormData.GetValues("latitude")[0];
            _request.longitude = provider.FormData.GetValues("longitude")[0];

            if (provider.FormData.AllKeys.Contains("accessToken"))
                _request.accessToken = provider.FormData.GetValues("accessToken")[0];

            BaseTask task = new ProcessingTask(_request, new CreateActivityResponse());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);

        }
    }
}

