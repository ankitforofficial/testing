﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class GroupUserController : BaseController
    {

        // GET: test
        [System.Web.Http.HttpPost]
        public IHttpActionResult Post([FromBody] GroupUserRequest request)
        {
            BaseTask task = new ProcessingTask(request, new GroupUserResponse());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);
        }
    }
}
