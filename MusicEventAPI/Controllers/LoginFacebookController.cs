﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class LoginFacebookController : BaseController
    {

        // GET: test
        [System.Web.Http.HttpPost]
        public async Task<IHttpActionResult> Post()
        {

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Content/BodyPart");
            var provider = new MultipartFormDataStreamProvider(root);
            LoginFacebookRequest _request = new LoginFacebookRequest();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (MultipartFileData file in provider.FileData)
            {

                //_request.filepath = file.LocalFileName;
                _request.FileName = file.LocalFileName;//file.Headers.ContentDisposition.FileName;

            }

            _request.fbid = provider.FormData.GetValues("fbid")[0];
            if (provider.FormData.AllKeys.Contains("emailId"))
                _request.emailId = provider.FormData.GetValues("emailId")[0];
            _request.userName = provider.FormData.GetValues("userName")[0];
            _request.name = provider.FormData.GetValues("name")[0];
            if (provider.FormData.AllKeys.Contains("deviceToken"))
                _request.deviceToken = provider.FormData.GetValues("deviceToken")[0];
            _request.deviceType = provider.FormData.GetValues("deviceType")[0];
            if (provider.FormData.AllKeys.Contains("filepath"))
                _request.filepath = provider.FormData.GetValues("filepath")[0];

            if (provider.FormData.AllKeys.Contains("isLogin"))
                _request.isLogin = provider.FormData.GetValues("isLogin")[0];

            if (provider.FormData.AllKeys.Contains("faceBookFriends"))
                _request.faceBookFriends = provider.FormData.GetValues("faceBookFriends")[0];

            BaseTask task = new ProcessingTask(_request, new LoginFacebookResponse());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);

        }
    }
}


