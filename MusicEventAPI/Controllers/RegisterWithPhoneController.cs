﻿using MusicEventAPI.Models.Request;
using MusicEventAPI.Models.Response;
using MusicEventAPI.Service;
using MusicEventAPI.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class RegisterWithPhoneController : BaseController
    {

        // GET: test
        [System.Web.Http.HttpPost]
        public IHttpActionResult Post([FromBody] RegisterWithPhoneRequest request)
        {
            BaseTask task = new ProcessingTask(request, new RegisterWithPhoneResponse());
            var response = task.CreateResponse();
            return base.ReturnModelState(response);
        }
    }
}
