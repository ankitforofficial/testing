﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MusicEventAPI.Controllers
{
    public class BaseController : ApiController
    {
        //protected string GetURL()
        //{
        //    return Request.RequestUri.AbsolutePath;
        //}

        internal IHttpActionResult ReturnHttpResponseMessage(Models.Response.BaseResponse response)
        {
            return Ok(response);
        }

        internal IHttpActionResult ReturnModelState(Models.Response.BaseResponse response)
        {
            return Ok(response.Result);
        }

    }
}
